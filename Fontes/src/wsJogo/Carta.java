
package wsJogo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de carta complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="carta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="forca" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="inteligencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="velocidade" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "carta", propOrder = {
    "forca",
    "inteligencia",
    "velocidade"
})
public class Carta {

    protected int forca;
    protected int inteligencia;
    protected int velocidade;

    /**
     * Obtém o valor da propriedade forca.
     * 
     */
    public int getForca() {
        return forca;
    }

    /**
     * Define o valor da propriedade forca.
     * 
     */
    public void setForca(int value) {
        this.forca = value;
    }

    /**
     * Obtém o valor da propriedade inteligencia.
     * 
     */
    public int getInteligencia() {
        return inteligencia;
    }

    /**
     * Define o valor da propriedade inteligencia.
     * 
     */
    public void setInteligencia(int value) {
        this.inteligencia = value;
    }

    /**
     * Obtém o valor da propriedade velocidade.
     * 
     */
    public int getVelocidade() {
        return velocidade;
    }

    /**
     * Define o valor da propriedade velocidade.
     * 
     */
    public void setVelocidade(int value) {
        this.velocidade = value;
    }

}
