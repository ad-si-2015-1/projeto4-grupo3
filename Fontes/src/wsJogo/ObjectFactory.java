
package wsJogo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the wsJogo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MostrarJogador_QNAME = new QName("http://jogo.com/", "mostrarJogador");
    private final static QName _RegistrarJogador_QNAME = new QName("http://jogo.com/", "registrarJogador");
    private final static QName _IniciarResponse_QNAME = new QName("http://jogo.com/", "iniciarResponse");
    private final static QName _RegistrarJogadorResponse_QNAME = new QName("http://jogo.com/", "registrarJogadorResponse");
    private final static QName _Iniciar_QNAME = new QName("http://jogo.com/", "iniciar");
    private final static QName _MostrarJogadorResponse_QNAME = new QName("http://jogo.com/", "mostrarJogadorResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: wsJogo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Iniciar }
     * 
     */
    public Iniciar createIniciar() {
        return new Iniciar();
    }

    /**
     * Create an instance of {@link MostrarJogadorResponse }
     * 
     */
    public MostrarJogadorResponse createMostrarJogadorResponse() {
        return new MostrarJogadorResponse();
    }

    /**
     * Create an instance of {@link RegistrarJogadorResponse }
     * 
     */
    public RegistrarJogadorResponse createRegistrarJogadorResponse() {
        return new RegistrarJogadorResponse();
    }

    /**
     * Create an instance of {@link IniciarResponse }
     * 
     */
    public IniciarResponse createIniciarResponse() {
        return new IniciarResponse();
    }

    /**
     * Create an instance of {@link MostrarJogador }
     * 
     */
    public MostrarJogador createMostrarJogador() {
        return new MostrarJogador();
    }

    /**
     * Create an instance of {@link RegistrarJogador }
     * 
     */
    public RegistrarJogador createRegistrarJogador() {
        return new RegistrarJogador();
    }

    /**
     * Create an instance of {@link Carta }
     * 
     */
    public Carta createCarta() {
        return new Carta();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MostrarJogador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://jogo.com/", name = "mostrarJogador")
    public JAXBElement<MostrarJogador> createMostrarJogador(MostrarJogador value) {
        return new JAXBElement<MostrarJogador>(_MostrarJogador_QNAME, MostrarJogador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrarJogador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://jogo.com/", name = "registrarJogador")
    public JAXBElement<RegistrarJogador> createRegistrarJogador(RegistrarJogador value) {
        return new JAXBElement<RegistrarJogador>(_RegistrarJogador_QNAME, RegistrarJogador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IniciarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://jogo.com/", name = "iniciarResponse")
    public JAXBElement<IniciarResponse> createIniciarResponse(IniciarResponse value) {
        return new JAXBElement<IniciarResponse>(_IniciarResponse_QNAME, IniciarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrarJogadorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://jogo.com/", name = "registrarJogadorResponse")
    public JAXBElement<RegistrarJogadorResponse> createRegistrarJogadorResponse(RegistrarJogadorResponse value) {
        return new JAXBElement<RegistrarJogadorResponse>(_RegistrarJogadorResponse_QNAME, RegistrarJogadorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Iniciar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://jogo.com/", name = "iniciar")
    public JAXBElement<Iniciar> createIniciar(Iniciar value) {
        return new JAXBElement<Iniciar>(_Iniciar_QNAME, Iniciar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MostrarJogadorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://jogo.com/", name = "mostrarJogadorResponse")
    public JAXBElement<MostrarJogadorResponse> createMostrarJogadorResponse(MostrarJogadorResponse value) {
        return new JAXBElement<MostrarJogadorResponse>(_MostrarJogadorResponse_QNAME, MostrarJogadorResponse.class, null, value);
    }

}
