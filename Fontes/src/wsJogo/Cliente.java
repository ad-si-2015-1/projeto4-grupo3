package wsJogo;


import wsJogo.Carta;
import wsJogo.ServidorJogo;
import wsJogo.ServidorJogo_Service;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
import java.util.Scanner;

public class Cliente {
   
    public static void main(String args[]) throws Exception {

   /*URL url  = new URL("http://localhost:9876/jogo.com?wsdl"); /*passa o endereco 
    do WSDL para o url, indicando onde o WSDL do servico podera ser encontrado.*/
    
    /*QName qname = new QName("http://jogo/","ServerService"); /* Passa o nome
    em XML para QName, indicando uma URI e o nome do servico.*/
    
    /*Service ws = Service.create(url,qname); /* Retorna uma referencia a um 
    objeto que pode invocar as operacoes oferecidas pelo servico.*/
    
    /*ServidorJogo server = ws.getPort(ServidorJogo.class);/*Acessa a interface
     endpoint do ServerInt, acessando os metodso*/
    
    ServidorJogo_Service ws = new ServidorJogo_Service();
    ServidorJogo server = ws.getServidorJogoPort();
    
        System.out.println("Qual é o seu nome ? ");
        Scanner scanner = new Scanner(System.in);
        String nome = scanner.next();
        String aux2 = server.registrarJogador(nome);
        
        
        if(!"O jogo já atingiu o numero maximo de jogadores.".equals(aux2)){
            System.out.println(aux2);
        
            int aux1 = 0;
            String aux3;
            while(aux1 == 0){
        
            aux3 = server.mostrarJogador(nome);
            if(!"Não há outro jogador conectado!".equals(aux3)){
                
                System.out.println(aux3);
                aux1 = 1;
            }
        
        }
        
        Carta carta = gerarCarta();
        System.out.println("Carta de " + nome);
        System.out.println("----------------------");
        System.out.println("FORCA: " +carta.getForca());
        System.out.println("INTELIGENCIA: " + carta.getInteligencia());
        System.out.println("VELOCIDADE: " + carta.getVelocidade());
        System.out.println("----------------------");
                
        String resultado = server.iniciar(carta);
        System.out.println(resultado);
        
        }
        else{
            System.out.println(server.registrarJogador(nome));
        }
        
    }    

    private static Carta gerarCarta() {
    
        int totalDePontos = 20;
        int forca = 0, inteligencia = 0, velocidade = 0;
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Distribua os pontos em atributos para sua carta!\n");
        
        boolean entradaCorreta = false;
        
        while(entradaCorreta == false) {
            System.out.println("Total de pontos: " + totalDePontos);
            System.out.println("Força: ");
            
            try {
                forca = scanner.nextInt();
            } catch (Exception e) {
                System.out.println("O valor deve ser um número!\n");
                continue;
            }
            if(forca <= totalDePontos) {
                entradaCorreta = true;
                totalDePontos -= forca;
            } else {
                System.out.println("O valor deve respeitar o total de pontos!\n");
            }
        }
        
        entradaCorreta = false;
        
        while(entradaCorreta == false) {
            System.out.println("Total de pontos: " + totalDePontos);
            System.out.println("Inteligência: ");
            
            try {
                inteligencia = scanner.nextInt();
            } catch (Exception e) {
                System.out.println("O valor deve ser um número!\n");
                continue;
            }
            if(inteligencia <= totalDePontos) {
                entradaCorreta = true;
                totalDePontos -= inteligencia;
            } else {
                System.out.println("O valor deve respeitar o total de pontos!\n");
            }
        }
        
        entradaCorreta = false;
        
        while(entradaCorreta == false) {
            System.out.println("Total de pontos: " + totalDePontos);
            System.out.println("Velocidade: ");
            
            velocidade = totalDePontos;
            entradaCorreta = true;
        }
        
        System.out.println("\n\n\n\n\n");
        
        Carta c = new Carta();
        c.setForca(forca);
        c.setInteligencia(inteligencia);
        c.setVelocidade(velocidade);
        return c;

    }
}
 