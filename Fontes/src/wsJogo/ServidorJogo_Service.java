
package wsJogo;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;
import com.jogo.ServidorJogo;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.10-b140803.1500
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "ServidorJogo", targetNamespace = "http://jogo.com/", wsdlLocation = "http://localhost:9876/jogo.com?wsdl")
public class ServidorJogo_Service
    extends Service
{

    private final static URL SERVIDORJOGO_WSDL_LOCATION;
    private final static WebServiceException SERVIDORJOGO_EXCEPTION;
    private final static QName SERVIDORJOGO_QNAME = new QName("http://jogo.com/", "ServidorJogo");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:9876/jogo.com?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SERVIDORJOGO_WSDL_LOCATION = url;
        SERVIDORJOGO_EXCEPTION = e;
    }

    public ServidorJogo_Service() {
        super(__getWsdlLocation(), SERVIDORJOGO_QNAME);
    }


    /**
     * 
     * @return
     *     returns ServidorJogo
     */
    @WebEndpoint(name = "ServidorJogoPort")
    public ServidorJogo getServidorJogoPort() {
        return super.getPort(new QName("http://jogo.com/", "ServidorJogoPort"), ServidorJogo.class);
    }

    private static URL __getWsdlLocation() {
        if (SERVIDORJOGO_EXCEPTION!= null) {
            throw SERVIDORJOGO_EXCEPTION;
        }
        return SERVIDORJOGO_WSDL_LOCATION;
    }

}
