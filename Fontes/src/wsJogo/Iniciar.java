
package wsJogo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de iniciar complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="iniciar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="carta" type="{http://jogo.com/}carta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "iniciar", propOrder = {
    "carta"
})
public class Iniciar {

    protected Carta carta;

    /**
     * Obtém o valor da propriedade carta.
     * 
     * @return
     *     possible object is
     *     {@link Carta }
     *     
     */
    public Carta getCarta() {
        return carta;
    }

    /**
     * Define o valor da propriedade carta.
     * 
     * @param value
     *     allowed object is
     *     {@link Carta }
     *     
     */
    public void setCarta(Carta value) {
        this.carta = value;
    }

}
