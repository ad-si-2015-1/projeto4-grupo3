
package com.jogo;


import java.io.Serializable;


public class Carta implements Serializable {
    
    int inteligencia;
    int forca;
    int velocidade;
    
    public Carta(){
        
        this.inteligencia = 0;
        this.forca = 0;
        this.velocidade = 0;
    }
    
    public Carta(int inteligencia, int forca, int velocidade) {
        this.inteligencia = inteligencia;
        this.forca = forca;
        this.velocidade = velocidade;
    }
    
    public int getInteligencia() {
        return inteligencia;
    }
    public int getForca() {
        return forca;
    }
    public int getVelocidade() {
        return velocidade;
    }

    public void setForca(int forca) {
        this.forca = forca;
    }

    public void setInteligencia(int inteligencia) {
        this.inteligencia = inteligencia;
    }

    public void setVelocidade(int velocidade) {
        this.velocidade = velocidade;
    }
    
}
