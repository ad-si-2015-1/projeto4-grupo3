package com.jogo;

import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;


/**
 *
 * @author Fred
 */
@WebService(serviceName = "ServidorJogo")

public class ServidorJogo {

    public String jog1 = null; // Jogador 1
    public String jog2 = null; // Jogador 2
    List<Carta> cartas = new ArrayList();

    /**
     * Operação de Web service
     * @param nome
     * @return 
     */
    @WebMethod(operationName = "registrarJogador")
    public String registrarJogador(@WebParam(name = "nome") String nome) {
         if (jog1 == null) {
            jog1 = nome;     
        } else {
            if (jog2 == null) {
                jog2 = nome;
            } else {
                return "O jogo já atingiu o numero maximo de jogadores.";
            }
        }
        return nome + ", aguarde o outro jogador.";
    }

    /**
     * Operação de Web service
     * @param nome
     * @return 
     */
    @WebMethod(operationName = "mostrarJogador")
    public String mostrarJogador(@WebParam(name = "nome") String nome) {
        if(jog1 == null || jog2 == null){
            
            return "Não há outro jogador conectado!";
        }
        else{
           if (jog1.equals(nome)) {
            return jog1 + ",você vai jogar com o(a) " + jog2;
        } else {
            return jog2 + ",você vai jogar com o(a) " + jog1;
        } 
       }
    }

    /**
     * Operação de Web service
     * @param carta
     * @return 
     */
    @WebMethod(operationName = "iniciar")
    public String iniciar(@WebParam(name = "carta") Carta carta) {
        
        

        cartas.add(carta);
        int aux = 0;
        int aux1 = 0;
        int player = 2;

        String ganhador = null;
         
        try {
            while (aux != 1) {

                if (cartas.size() == 2) {

                    int aux2 = compare();

                    if (aux2 == 0) {
                        ganhador = "Empate!";
                    } else if (player == aux2) {
                        ganhador = "Voce ganhou!";
                    } else {
                        ganhador = "Voce perdeu!";
                    }

                    aux = 1;
                    aux1++;
                    if (aux1 == 2) {
                        cartas.clear();
                    }
                } else {

                    player = 1;
                    aux = 0;
                }
                

                synchronized (this) {
                    this.wait(1000);

                }

            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return ganhador;
    }

    private int compare() {
        
        Carta carta1 =cartas.get(0);
        Carta carta2 = cartas.get(1);
        int p1 = 0;
        int p2 = 0;

        if (carta1.getForca() > carta2.getForca()) {
            p1++;
        } else if (carta1.getForca() < carta2.getForca()) {
            p2++;
        }

        if (carta1.getInteligencia() > carta2.getInteligencia()) {
            p1++;
        } else if (carta1.getInteligencia() < carta2.getInteligencia()) {
            p2++;
        }

        if (carta1.getVelocidade() > carta2.getVelocidade()) {
            p1++;
        } else if (carta1.getVelocidade() < carta2.getVelocidade()) {
            p2++;
        }

        if (p1 > p2) {
            return 1;
        } else if (p1 < p2) {
            return 2;
        } else {
            return 0;
        }
    }
    
}
